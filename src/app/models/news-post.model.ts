export interface NewsPost {
    title: string;
    url?: string;
    score?: number;
    by?: string;
    domain?: string;
    time?: number;
}
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDetailsComponent } from './new-details.component';

describe('NewDetailsComponent', () => {
  let component: NewDetailsComponent;
  let fixture: ComponentFixture<NewDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('comment should be a string', () => {
    expect(component.comment).toEqual('');
  });

  it('should fetch the post details.', (() => {
    spyOn(component, "ngOnInit").and.callThrough();
    component.ngOnInit();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.newsDetails).toBeTruthy();
    });
  }));
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NewsPostService } from '../news-post.service';
import { NewsPost } from 'src/app/models/news-post.model';

@Component({
  selector: 'app-new-details',
  templateUrl: './new-details.component.html',
  styleUrls: ['./new-details.component.scss']
})
export class NewDetailsComponent implements OnInit {
  id: number = 0;
  newsDetails!: NewsPost;
  comment: string = '';
  time: string = '';
  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private newSrv: NewsPostService
  ) { }

  /**
   * fetching news data using the id in the route params
   */
  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.id = parseInt(params.get('id')!);
      if (this.id) {
        this.newSrv.getNewsDetails(this.id).subscribe(resDetails => {
          if (resDetails) {
            this.newsDetails = resDetails;
            if (this.newsDetails?.time) {
              this.time = this.newSrv.convertTime(this.newsDetails.time);
            }
          }
        })
      }
    })
  }

  gotoNewsList() {
    this.router.navigate(['']);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { environment } from '../../../environments/environment';
import { AppConfig } from 'src/app/config/app-config';
import { Observable } from 'rxjs';
import { NewsPost } from 'src/app/models/news-post.model';

@Injectable({
  providedIn: 'root'
})
export class NewsPostService {
  urlPrefix = environment.endpoints;
  constructor(private http: HttpClient) {
  }
  /**
    * GetLatestNews is used to get all news in a list.
    * @returns Array of News as Observable
    */
  getLatestNews(): Observable<number[]> {
    let URL = `${this.urlPrefix}${AppConfig.ENDPOINTS.GET_NEW_LIST}?print=pretty`
    return this.http.get<number[]>(URL)
  }
  /**
    * GetNewsDetails is used to get detail of news according to the id.
    * @param id is a item id for which detail has to be fetch.
    * @returns json object which contain news details as Observable
    */
  getNewsDetails(id: number): Observable<NewsPost> {
    let URL = `${this.urlPrefix}${AppConfig.ENDPOINTS.GET_NEWS_DETAILS}/${id}.json?print=pretty`
    return this.http.get<NewsPost>(URL)
  }

  /**
   * Function to convert time from milliseconds to hours and minutes
   * @param timeInMs time in milliseconds
   * @returns time in hours + minutes
   */
  convertTime(timeInMs: number) {
    let minutes: any = Math.floor((timeInMs / (1000 * 60)) % 60),
    hours: any = Math.floor((timeInMs / (1000 * 60 * 60)) % 24);
    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    return (hours > 0 ? hours + " hours " : '') + (minutes > 0 ? minutes + " minutes " : '');
  }
}

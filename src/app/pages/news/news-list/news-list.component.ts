import { Component, OnInit } from '@angular/core';
import { NewsPostService } from '../news-post.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NewsPost } from 'src/app/models/news-post.model';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {
  pageNo: number = 0; // page number is starting from 0
  pageLimit: number = 20; //default page limit is 20
  allNewsPosts: number[] = [];
  newsToDisplay: NewsPost[] = [];;
  constructor(private newSrv: NewsPostService, private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getNewPost()
  }
  /**
  * getNewList function is used to get all the news posts
  */
  getNewPost() {
    this.newSrv.getLatestNews().subscribe((res: number[]) => {
      if (res?.length) {
        this.allNewsPosts = res;
        this.getNewsDetails(this.allNewsPosts);
      }
    })
  }
  /**
   * This function will get the data of the news post according to the pagination
   * We will add the records according to the pagination in the newsToDisplay array
   * @param data is array of news list
   */
  async getNewsDetails(data: number[]) {
    let firstIndex: any = this.pageLimit * this.pageNo
    let secondIndex: any = this.pageNo === 0 ? this.pageLimit : this.pageLimit + this.pageNo * this.pageLimit
    for (let i: any = firstIndex; i < secondIndex; i++) {
      await this.newSrv.getNewsDetails(data[i]).subscribe((resDetails: NewsPost) => {
        if (resDetails) {
          if (resDetails?.url) {
            let domain: any = (new URL(resDetails?.url));
            domain = domain.hostname;
            if (domain?.includes('www.'))
              domain = domain.replace('www.','');
            resDetails.domain = domain;
          }
          this.activatedRoute.queryParams.subscribe(params => {
            const filter = params['site'];
            if (filter) {
              if (resDetails?.url?.includes(filter)) {
                this.newsToDisplay.push(resDetails);
              }
            } else {
              this.newsToDisplay.push(resDetails);
            }
          })
        }
      })
    }
  }

  /**
  * This function will redirect to the detail page of the post if it exist
  * if the detail page url does not exist then it will be routed to our default detail page
  * @param list details of the post
  */
  gotoDetailPage(list: any) {
    if (list.url) {
      window.location.href = list.url;
    }
    else {
      this.router.navigate([`/details/${list.id}`])
    }
  }

  /**
  * OnScroll function is used to perform infinite scroll
  */
  onScroll() {
    this.pageNo = this.pageNo + 1;
    this.getNewsDetails(this.allNewsPosts);
  }

  /**
   * show records on the basis of domain
   * @param domain
   */
  filterRecords(domain: string | undefined) {
    if (!domain) return;
    this.router.navigate(['/from'], {queryParams: {site: domain}});
  }
}
